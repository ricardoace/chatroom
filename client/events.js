Template.welcome.events({
    "submit .name-form": function (event) {
        event.preventDefault();
        var name = event.target.firstname.value;
        
        Posts.insert({
            name: name
        });
        
    }
});
Template.post.events({
    "click .post-class": function (){
        Router.go('home');
    }
});